FROM amazon/aws-cli

RUN yum update -y && \
	yum install -y jq tar cronie && \
	amazon-linux-extras install docker

# setup cron
RUN sed -i -e '/pam_loginuid.so/s/^/#/' /etc/pam.d/crond
COPY backup.cron /etc/cron.d/backup.template
RUN chmod 0644 /etc/cron.d/backup.template
ENV BACKUP_CRON_SCHEDULE="1 0 * * *"

# setup tools
COPY tools /root/bin
RUN chmod +x -R /root/bin
ENV PATH="${PATH}:/root/bin"

# setup backup dir & files
COPY backup-placeholder.sh /scripts/backup.sh
RUN chmod +x /scripts/backup.sh
COPY restore-placeholder.sh /scripts/restore.sh
RUN chmod +x /scripts/restore.sh

RUN mkdir /backup
WORKDIR /backup

ENTRYPOINT []

CMD eval "echo \"$(cat /etc/cron.d/backup.template)\"" > /etc/cron.d/backup && \
	cat /etc/cron.d/backup && \
	crontab /etc/cron.d/backup && \
	crond -n
