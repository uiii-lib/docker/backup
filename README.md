# Backup docker image

## Backup and restore scripts
Backup script is located at `/scripts/backup.sh` and is called with a single parameter containing a path to a folder where the backup files should be placed.

Restore script is located at `/scripts/restore.sh` and is called with a single parameter containing a path to a folder with backup files.

So, copy your scripts to these locations when creating new docker image or mount them as volumes.

## Environment variables

- `AWS_REGION` - AWS region (**required on AWS**, e.g. `eu-central-1`)
- `AWS_CLUSTER` - name of the AWS cluster (**required on AWS**)
- `BACKUP_S3_BUCKET` - name of the S3 bucket where are the backups uploaded (**required**)
- `BACKUP_S3_PREFIX` - backup archives will be prefixed by this string like *prefix-2021-01-01-00-00.tar*  (**required**)
- `BACKUP_RETENTION_CLASSES` - list of retention classes (e.g. `"3 2 1"`), by default each backup has retention class *1*
- `BACKUP_RETENTION_RANGES` - list of retention class ranges (e.g. `"21 7 1"`), day number since epoch is moduled by this range
- `BACKUP_RETENTION_OFFSETS` - list of retention class offsets (e.g. `"2 2 0"`), result of a day number moduled by the range must be equal to this offset for the class to be applied
- `BACKUP_CRON_SCHEDULE` - cron schedule expression when the backup is performed, use `#` to disable backup (default `"1 0 * * *"`)

## AWS authentication

Image uses AWS CLI command so it needs to be authenticated. When running on AWS container instance it works out of the box, but in other environments you have to use either [environment variables](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html) or mount your `.aws` folder with credentials as a volume to `/root/.aws` inside the container, don't forget to set `AWS_PROFILE` env var if you are using profiles.
